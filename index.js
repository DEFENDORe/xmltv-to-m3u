const libxml = require("libxmljs")
const fs = require('fs')

const XML_FILE = "./xmltv.xml"
const VIDEO_URL = "http://localhost:8181/video?channel="
const M3U_OUTPUT = "./channels.m3u"

// Read the XML data
console.log("Parsing XMLTV Channels.. This may take a moment.")
var xml =  fs.readFileSync(XML_FILE)
var xmlDoc = libxml.parseXmlString(xml)
var channels = xmlDoc.get('/tv').find('channel')
console.log("Total number of channels found: " + channels.length)

// Build M3U from channel elements..
console.log("Building M3U Data...")
var m3u = "#EXTM3U\n"
channels.forEach((channel, i) => {
    /*
    A channel entry from a mc2xml XMLTV file using zap2it..

    <channel id="I8.10108.zap2it.com">
        <display-name>8 CFTO</display-name>
        <display-name>8</display-name>
        <display-name>CFTO</display-name>
        <icon src="https://zap2it.tmsimg.com/h3/NowShowing/10108/s10108_h3_aa.png" />
    </channel>
    */
    var displayNames = channel.find('display-name')
    var channelNumber = displayNames[1].text()
    var channelName = displayNames[2].text()

    m3u += "\n#EXTINF:0 " + channelNumber + " - " + channelName + "\n" + VIDEO_URL + channelNumber + "\n"
})

// Write the M3U File
console.log("Writing M3U File")
fs.writeFile(M3U_OUTPUT, m3u, (err) => {
    if (err) throw err
    console.log("M3U File Saved!")
})